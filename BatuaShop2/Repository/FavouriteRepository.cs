﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BatuaShop2.Interfaces;
using BatuaShop2.Models;
using Microsoft.EntityFrameworkCore;

namespace BatuaShop2.Repository
{
    public class FavouriteRepository : IFavourite
    {
        
        private readonly AppDbContent _appDbContent;

        public FavouriteRepository(AppDbContent appDbContent)
        {
            _appDbContent = appDbContent;
        }
        
        public Task<List<Favourites>> getFavourites(string userId)
        {
            return Task.FromResult(_appDbContent.Favourites.Where(f => f.userId.Equals(userId)).ToList());
        }

        public async Task<bool> addFavourite(string userId, int bookId)
        {
            var found = _appDbContent.Favourites.FirstOrDefault(f => f.bookId == bookId && f.userId.Equals(userId));
            if (found==null)
            {
                var favourite = new Favourites()
                {
                    userId = userId,
                    bookId = bookId
                };
                await AddFav(favourite);
            }
            else
            {
                await RemoveFav(found);
                return false;
            }
            await _appDbContent.SaveChangesAsync();
            return true;
        }

        private async Task AddFav(Favourites fav)
        {
            await _appDbContent.Favourites.AddAsync(fav);
            await _appDbContent.SaveChangesAsync();
        }

        private async Task RemoveFav(Favourites fav)
        {
            _appDbContent.Favourites.Remove(fav);
            await _appDbContent.SaveChangesAsync();
        }

    }
}