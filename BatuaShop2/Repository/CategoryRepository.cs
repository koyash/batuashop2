﻿using System.Collections.Generic;
using BatuaShop2.Data;
using BatuaShop2.Interfaces;
using BatuaShop2.Models;

namespace BatuaShop2.Repository
{
    public class CategoryRepository : IBookCategory
    {

        private readonly AppDbContent appDbContent;

        public CategoryRepository(AppDbContent appDbContent)
        {
            this.appDbContent = appDbContent;
        }

        public IEnumerable<Category> allCategories => appDbContent.Category;

    }
}
