﻿using System.Collections.Generic;
using System.Linq;
using BatuaShop2.Data;
using BatuaShop2.Interfaces;
using BatuaShop2.Models;
using Microsoft.EntityFrameworkCore;

namespace BatuaShop2.Repository
{
    public class BookRepository : IAllBooks
    {

        private AppDbContent appDbContent;

        public BookRepository(AppDbContent appDbContent)
        {
            this.appDbContent = appDbContent;
        }

        public IEnumerable<Book> Books => appDbContent.Book;



        public Book getObjectBook(int bookId) => appDbContent.Book.FirstOrDefault(p => p.id == bookId);

        public void RemoveFromFavourite(int bookId)
        {
            var foundBook = appDbContent.Book.FirstOrDefault(b => b.id == bookId);
            appDbContent.Book.Update(foundBook);
            appDbContent.SaveChanges();
        }
        public void AddToFavourite(int bookId)
        {
            var foundBook = appDbContent.Book.FirstOrDefault(b => b.id == bookId);
            appDbContent.Book.Update(foundBook);
            appDbContent.SaveChanges();
        }

        public List<Book> getBooksByName(string name) =>  appDbContent.Book.Where(x => x.name.Contains(name)).ToList();
    }
}
