﻿using System.Linq;
using System.Threading.Tasks;
using BatuaShop2.Data;
using BatuaShop2.Interfaces;
using BatuaShop2.Models;

namespace BatuaShop2.Repository
{
    public class EmailRepository : IEmail
    {
        
        
        private readonly AppDbContent _appDbContent;

        public EmailRepository(AppDbContent appDbContent)
        {
            this._appDbContent = appDbContent;
        }
        
        public async Task<bool> SaveEmail(Email email)
        {
            var existingEmail = _appDbContent.Email.Any(e => e.email.Equals(email.email));
            if (!existingEmail)
            {
                await _appDbContent.Email.AddAsync(email);
                await _appDbContent.SaveChangesAsync();
                return true;
            }
            return false;
        }
    }
}