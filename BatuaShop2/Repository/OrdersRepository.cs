﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BatuaShop2.Interfaces;
using BatuaShop2.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BatuaShop2.Repository
{
    public class OrdersRepository : IAllOrders
    {

        private readonly AppDbContent appDbContent;
        private readonly ShopCart shopCart;

        public OrdersRepository(AppDbContent appDbContent, ShopCart shopCart, UserManager<Users> userManager)
        {
            this.appDbContent = appDbContent;
            this.shopCart = shopCart;
        }

        public async Task CreateOrder(Order order)
        {
            order.dateTime = DateTime.Now; 
            var o = appDbContent.Order.AddAsync(order).Result;
            appDbContent.SaveChanges();
            var items = ShopCart.listShopItems;

            List<OrderDetail> details = new List<OrderDetail>();
            
            foreach(var el in items)
            {
                var orderDetail = new OrderDetail()
                {
                    bookId = el.book.id,
                    orderId = order.id,
                    price = el.book.price
                };
                details.Add(orderDetail);
                await appDbContent.OrderDetail.AddAsync(orderDetail);
            }

            order.orderDetails = details;
            appDbContent.Order.Update(order);
            appDbContent.SaveChanges();
        }

        public async Task<bool> CloseOrder(int id)
        {
            var foundOrder = await appDbContent.Order.FirstAsync(o => o.id == id);
            if (foundOrder != null)
            {
                foundOrder.status = 0;
                appDbContent.Order.Update(foundOrder);
                await appDbContent.SaveChangesAsync();
                return true;
            }

            return false;
        }
    }
}
