using BatuaShop2.Interfaces;
using BatuaShop2.Models;
using BatuaShop2.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using static BatuaShop2.Data.DbObjects;

namespace BatuaShop2
{
    public class Startup
    {
        private IConfigurationRoot confString;

        public Startup(IConfiguration configuration)
        {
            confString = (IConfigurationRoot) configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppDbContent>(options =>
                options.UseSqlServer(confString.GetConnectionString("DefaultConnection")));

            services.AddIdentity<Users, IdentityRole>(config =>
                {
                    config.Password.RequiredLength = 4;
                    config.Password.RequireDigit = false;
                    config.Password.RequireNonAlphanumeric = false;
                    config.Password.RequireUppercase = false;
                })
                .AddEntityFrameworkStores<AppDbContent>()
                .AddDefaultTokenProviders();
            
            services.ConfigureApplicationCookie(config =>
            {
                config.Cookie.Name = "Auth.Cookie";
                config.LoginPath = "/Authorization/Login";
            });

            services.AddAuthorization(config =>
            {
                var defaultAuthBilder = new AuthorizationPolicyBuilder();
                var defaultAuthPolicy = defaultAuthBilder
                    .RequireAuthenticatedUser()
                    .Build();
                config.DefaultPolicy = defaultAuthPolicy;
            });
            
            services.AddTransient<IAllBooks, BookRepository>();
            services.AddTransient<IBookCategory, CategoryRepository>();
            services.AddTransient<IAllOrders, OrdersRepository>();
            services.AddTransient<IEmail, EmailRepository>();
            services.AddTransient<IFavourite, FavouriteRepository>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped(ShopCart.GetCart);
            services.AddMemoryCache();
            services.AddSession();
            services.AddMvc();
            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseStatusCodePagesWithRedirects("/Error/{0}");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSession();

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();


            app.UseAuthentication();
            app.UseAuthorization();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            using var scope = app.ApplicationServices.CreateScope();
            var content = scope.ServiceProvider.GetRequiredService<AppDbContent>();
            Initial(content);
        }
    }
}