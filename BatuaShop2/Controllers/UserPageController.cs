﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BatuaShop2.Interfaces;
using BatuaShop2.Models;
using BatuaShop2.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BatuaShop2.Controllers
{
    public class UserPageController : Controller
    {
        private readonly UserManager<Users> _userManager;
        private AppDbContent _appDbContent;
        private readonly IAllBooks allBooks;
        private IFavourite _favourites;
        private IAllOrders _orders;

        private Users u;

        public UserPageController(UserManager<Users> userManager, AppDbContent appDbContent, IFavourite favourites,
            IAllBooks books, IAllOrders orders)
        {
            this._favourites = favourites;
            _userManager = userManager;
            _appDbContent = appDbContent;
            allBooks = books;
            _orders = orders;
        }

        // GET
        [Authorize]
        public IActionResult Page()
        {
            u = _userManager.GetUserAsync(User.Clone()).Result;

            List<Order> allorders = _appDbContent.Order.Where(o => o.userId.Equals(u.Id)).ToList();

            List<OrderDetail> orderDetails = _appDbContent.OrderDetail.ToList();

            List<Order> orders = new List<Order>();

            foreach (var order in allorders)
            {
                var v = orderDetails.Where(o => o.orderId == order.id).ToList();
                if (v.Count != 0)
                {
                    for (int i = 0; i < v.Count; i++)
                    {
                        if (v[i].book == null)
                        {
                            v[i].book = _appDbContent.Book.First(b => b.id == v[i].bookId);
                        }
                    }

                    orders.Add(new Order
                    {
                        id = order.id,
                        orderDetails = v,
                        status = order.status
                    });
                }
            }
            var ords = orders.Where(o => o.orderDetails.Count != 0).ToList();
            List<Order> ordersWithoutRepeat = new List<Order>();
            List<int> repeats = new List<int>();
            while (ords.Count > 0)
            {
                ordersWithoutRepeat.Add(ords[0]);
                int r = ords.RemoveAll(o => o.id == ords[0].id);
                repeats.Add(r);
            }
            var userPageVm = new UserPageViewModel()
            {
                User = u,
                orders = ordersWithoutRepeat,
                Repeats = repeats
            };

            return View(userPageVm);
        }

        [Authorize]
        public IActionResult Favourite()
        {
            u = _userManager.GetUserAsync(User.Clone()).Result;
            Task<List<Favourites>> favourites = _favourites.getFavourites(u.Id);
            List<Book> favBooks = new List<Book>();
            foreach (var fav in favourites.Result)
            {
                favBooks.Add(allBooks.getObjectBook(fav.bookId));
            }

            var result = new FavouritesViewModel()
            {
                favourites = favBooks
            };
            return View(result);
        }

        public async Task<IActionResult> ManagerPage()
        {
            var u = await _userManager.GetUserAsync(User.Clone());
            if (u != null)
            {
                var check = _appDbContent.UserRoles.FirstOrDefault(r => r.UserId.Equals(u.Id) && r.RoleId.Equals("1"));
                if (check != null)
                {
                    List<Order> allorders = _appDbContent.Order.Where(o=>o.status == 1).ToList();

                    List<OrderDetail> orderDetails = _appDbContent.OrderDetail.ToList();

                    List<Order> orders = new List<Order>();

                    foreach (var order in allorders)
                    {
                        var v = orderDetails.Where(o => o.orderId == order.id).ToList();
                        if (v.Count != 0)
                        {
                            for (int i = 0; i < v.Count; i++)
                            {
                                if (v[i].book == null)
                                {
                                    v[i].book = _appDbContent.Book.First(b => b.id == v[i].bookId);
                                }
                            }
                            orders.Add(new Order
                            {
                                id = order.id,
                                orderDetails = v,
                                name = order.name,
                                surname = order.surname,
                                email = order.email
                            });
                        }
                    }
                    var managerInfo = new ManagerInfoViewModel()
                    {
                        Users = u,
                        Orders = orders.Where(o=>o.orderDetails.Count != 0).ToList()
                    };
                    return View(managerInfo); 
                }
                return RedirectToAction("Page");
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<bool> CloseOrder(int id)
        {
            return await _orders.CloseOrder(id);
        }
    }
    
}