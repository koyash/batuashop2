﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BatuaShop2.Interfaces;
using BatuaShop2.Models;
using BatuaShop2.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BatuaShop2.Controllers
{
    public class OneBookController : Controller
    {
        private readonly IAllBooks allBooks;
        private readonly ShopCart _shopCart;
        private readonly IFavourite _favourite;
        private readonly UserManager<Users> _userManager;

        public OneBookController(IAllBooks allBooks, ShopCart shopCart, IFavourite favourite, UserManager<Users> userManager)
        {
            this.allBooks = allBooks;
            this._shopCart = shopCart;
            this._favourite = favourite;
            _userManager = userManager;
        }

        public async Task<ViewResult> GetOneBook(int id)
        {
            var selectedBook = allBooks.getObjectBook(id);

            List<Book> sameBooks = allBooks.Books.Where(b =>
                (b?.categoryId == selectedBook.categoryId || b.categoryName != null && b.categoryName.Equals(selectedBook.categoryName))
                && b.id != selectedBook.id).ToList();

            var u = await _userManager.GetUserAsync(User.Clone());
            bool isFavourite = false;

            if (u != null)
            {
                var favs = await _favourite.getFavourites(u.Id);
                var thisBookIsFavourite = favs.Where(f => f.bookId == id);
                if (thisBookIsFavourite.Any())
                {
                    isFavourite = true;
                }
            }
            
            var oneBook = new OneBookViewModel
            {
                book = selectedBook,
                sameBooks = sameBooks.Count == 0 ? sameBooks : sameBooks.Take(Math.Min(sameBooks.Count, 3)).ToList(),
                isFavourite = isFavourite
            };


            return View(oneBook);
        }

        public RedirectToActionResult AddToCart(int id)
        {
            var item = allBooks.Books.FirstOrDefault(i => i.id == id);
            if (item != null)
            {
                _shopCart.AddToCart(item);
            }

            return RedirectToAction("GetOneBook", new {id = id});
        }
    }
}