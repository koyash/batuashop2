﻿using System.Linq;
using BatuaShop2.Interfaces;
using BatuaShop2.Models;
using BatuaShop2.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace BatuaShop2.Controllers
{
    public class ShopCartController : Controller
    {
        private readonly IAllBooks bookRep;
        private readonly ShopCart shopCart;

        public ShopCartController(IAllBooks newBookRep, ShopCart newShopCart)
        {
            bookRep = newBookRep;
            shopCart = newShopCart;
        }

        public ViewResult Index()
        {
            var items = shopCart.getShopItems();
            ShopCart.listShopItems = items;

            var obj = new ShopCartViewModel
            {
                shopCart = shopCart
            };
            return View(obj);
        }

        public void AddToCart(int id)
        {
            var item = bookRep.Books.FirstOrDefault(i => i.id == id);
            if (item != null)
            {
                shopCart.AddToCart(item);
            }
        } 
    }
}