﻿using System.Threading.Tasks;
using BatuaShop2.Interfaces;
using BatuaShop2.Models;
using BatuaShop2.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace BatuaShop2.Controllers
{
    public class FooterController : Controller
    {
        private readonly IEmail _email;

        public FooterController(IEmail email)
        {
            _email = email;
        }

        public IActionResult SaveEmail(string email)
        {
            var em = new Email {email = email};
            Task<bool> savedResult = _email.SaveEmail(em);

            var saved = new FooterViewModel()
            {
                Saved = savedResult.Result
            };

            return View(saved);
        }
    }
}