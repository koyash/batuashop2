﻿using Microsoft.AspNetCore.Mvc;

namespace BatuaShop2.Controllers
{
    public class ErrorController : Controller
    {
        [Route("Error/{statusCode}")]
        public IActionResult Index(int statusCode)
        {
            switch (statusCode)
            {
                case 404:
                    ViewBag.ErrorMessage = "Page not found";
                    break;
            }
            return View();
        }
    }
}