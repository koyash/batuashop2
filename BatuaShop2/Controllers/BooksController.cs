﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BatuaShop2.Interfaces;
using BatuaShop2.Models;
using BatuaShop2.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BatuaShop2.Controllers
{
    public class BooksController : Controller
    {
        private readonly IAllBooks allBooks;
        private readonly IBookCategory allCategories;
        private readonly ShopCart _shopCart;
        private readonly UserManager<Users> _userManager;
        private readonly IFavourite _favourites;
        private int MaxBooksCount = 4;

        public BooksController(IAllBooks allBooks, IBookCategory bookCategory, ShopCart shopCart,
            UserManager<Users> userManager,
            IFavourite favourites)
        {
            this.allBooks = allBooks;
            this._favourites = favourites;
            this.allCategories = bookCategory;
            this._shopCart = shopCart;
            _userManager = userManager;
        }

        [Route("Books/List/")]
        [Route("Books/List/{category}")]
        [Route("Books/List/{category}/{page}")]
        public ViewResult List(string category = null, int page = 1)
        {
            List<Book> books = null;
            string bookCategory = "";
            var cat = allCategories.allCategories.FirstOrDefault(c => category != null && category.Equals(c.id.ToString()));
            if (cat != null)
            {
                books = allBooks.Books.Where(i => i.categoryId == cat.id || i.categoryName != null && i.categoryName.Equals(cat.categoryName))
                    .OrderBy(i => i.id).ToList();
                foreach (var book in books.ToList())
                {
                    book.categoryName = cat.categoryName;
                }
                bookCategory = category;
            }
            else
            {
                books = allBooks.Books.OrderBy(i => i.id).ToList();
            }

            List<int> pages = new List<int>();

            var bookCounter = (page - 1) * MaxBooksCount;
            
            if (page != 1)
                pages.Add(page-1);
            pages.Add(page);
            if (bookCounter + MaxBooksCount< books.Count())
                pages.Add(page + 1);

            var bookObject = new BooksListViewModel
            {
                allBooks = books.GetRange(bookCounter, Math.Min(MaxBooksCount, books.Count()-bookCounter)),
                currCategory = cat != null ? cat.id : 0,
                currCategoryName = cat != null ? cat.categoryName : "Все книги",
                pageNumbers = pages.ToArray(),
                selectedPage = page
            };

            ViewBag.Title = "Страница с книгами";
            return View(bookObject);
        }

        public ViewResult findBook(string searching)
        {
            IEnumerable<Book> books = null;
            searching.ToLower();
            books = allBooks.Books.Where(b => b.name.ToLower().StartsWith(searching));
            var bookObject = new BooksListViewModel
            {
                allBooks = books.ToList(),
                currCategory = 0
            };
            return View(bookObject);
        }

        [HttpPost]
        public bool AddToCart(int id)
        {
            var item = allBooks.Books.FirstOrDefault(i => i.id == id);
            if (item != null)
            {
                _shopCart.AddToCart(item);
                return true;
            }

            return false;
        }

        public async Task<bool> AddToFavourite(int bookId)
        {
            var u = _userManager.GetUserAsync(User.Clone()).Result;
            bool res = false;
            if (u != null)
            {
                res = await _favourites.addFavourite(u.Id, bookId);
            }
            return res;
        }
    }
}