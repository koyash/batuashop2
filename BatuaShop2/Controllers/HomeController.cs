﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BatuaShop2.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BatuaShop2.Models;
using BatuaShop2.ViewModels;

namespace BatuaShop2.Controllers
{
    public class HomeController : Controller
    {
        private IAllBooks bookRep;

        public HomeController(IAllBooks newBookRep)
        {
            bookRep = newBookRep;
        }

        public ViewResult Index()
        {
            return View();
        }

        public RedirectToActionResult RemoveFromFav(int id)
        {
            bookRep.RemoveFromFavourite(id);
            return RedirectToAction("Index");
        }
    }
}