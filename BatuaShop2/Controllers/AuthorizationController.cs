﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BatuaShop2.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BatuaShop2.Controllers
{
    public class AuthorizationController : Controller
    {
        private readonly UserManager<Users> _userManager;
        private readonly SignInManager<Users> _signInManager;
        private AppDbContent _appDbContent;

        public AuthorizationController(UserManager<Users> userManager,
            SignInManager<Users> signInManager, AppDbContent appDbContent)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _appDbContent = appDbContent;
        }

        // GET
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public IActionResult Secret()
        {
            return View();
        }
        
        [HttpPost]
        public async Task<IActionResult> Login(string email, string password)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user != null)
            {
                var result = await _signInManager.PasswordSignInAsync(user, password, false, false);
                if (result.Succeeded)
                {
                    var checkForManager = _appDbContent.UserRoles.FirstOrDefault(r => r.UserId.Equals(user.Id));
                    if (checkForManager != null)
                    {
                        return RedirectToAction("ManagerPage","UserPage"); 
                    }
                    return RedirectToAction("Page", "UserPage");
                }
            }

            return RedirectToAction("Login");
        }

        public IActionResult Login()
        {
            var u =  _userManager.GetUserAsync(User.Clone()).Result;
           
            if (u != null)
            {
                var checkForManager = _appDbContent.UserRoles.Where(r => r.UserId.Equals(u.Id)); if (checkForManager.Any())
                {
                    return RedirectToAction("ManagerPage","UserPage"); 
                }
                return RedirectToAction("Page", "UserPage");
            }
            return View();
        }

        [HttpPost]
        public async Task<RedirectToActionResult> Registration(string name, string password, string email, string secondName,
            string lastName)
        {
            var user = new Users()
            {
                UserName = email,
                Name = name,
                Email = email,
                SecondName = secondName,
                LastName = lastName
            };
            var result = await _userManager.CreateAsync(user, password);
            
            return RedirectToAction("Login");
        }

        public async Task<IActionResult> LogOut()
        {
            var signOut = _signInManager.SignOutAsync().IsCompleted;
            if (signOut)
                return RedirectToAction("Index", "Home");
            return RedirectToAction("Login");
        }

        public IActionResult Registration()
        {
            return View();
        }
    }
}