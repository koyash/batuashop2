﻿using System.Collections.Generic;
using BatuaShop2.Interfaces;
using BatuaShop2.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BatuaShop2.Controllers
{
    public class OrderController : Controller
    {
        private readonly IAllOrders allOrders;
        private readonly ShopCart _shopCart;
        private readonly UserManager<Users> _userManager;

        public OrderController(IAllOrders allOrders, ShopCart shopCart, UserManager<Users> userManager)
        {
            this.allOrders = allOrders;
            this._shopCart = shopCart;
            _userManager = userManager;
        }

        public IActionResult Checkout()
        {
            var u = _userManager.GetUserAsync(User.Clone()).Result;
            if (u != null)
            {
                return RedirectToAction("Complete");
            }
            return View();
        }

        [HttpPost]
        public IActionResult Checkout(Order order)
        {
            var u = _userManager.GetUserAsync(User.Clone()).Result;
            if (u != null)
            {
                order.userId = u.Id;
            }
            ShopCart.listShopItems = _shopCart.getShopItems();
            if (ShopCart.listShopItems.Count == 0)
            {
                ModelState.AddModelError("", "У вас должны быть товары");
            }
            if (ModelState.IsValid)
            {
                allOrders.CreateOrder(order);
                return RedirectToAction("Complete");
            }
            return View(order);
        }

        public IActionResult Complete(Order order)
        {
            ViewBag.Message = "Заказ успешно обработан";
            var u = _userManager.GetUserAsync(User.Clone()).Result;
            if (u != null)
            {
                order.userId = u.Id;
                order.adress = "";
                order.email = u.Email;
                order.name = u.Name;
                order.surname = u.SecondName;
                order.phone = "";
            }

            order.orderDetails = new List<OrderDetail>();

            if (ShopCart.listShopItems != null)
                foreach (var item in ShopCart.listShopItems)
                {
                    order.orderDetails.Add(new OrderDetail()
                    {
                        bookId = item.book.id,
                        price = item.price * item.count
                    });
                }

            if (order.orderDetails.Count == 0)
                return View();
            
            allOrders.CreateOrder(order);

            _shopCart.ClearShopItems();
            return View();
        }
    }
}