﻿using System.Collections;
using System.Collections.Generic;
using BatuaShop2.Models;

namespace BatuaShop2.ViewModels
{
    public class FavouritesViewModel
    {
        public List<Book> favourites { get; set; }
    }
}