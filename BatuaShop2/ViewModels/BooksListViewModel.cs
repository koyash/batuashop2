﻿using System.Collections.Generic;
using BatuaShop2.Models;

namespace BatuaShop2.ViewModels
{
    public class BooksListViewModel
    {
        public List<Book> allBooks { get; set; }
        public int currCategory { get; set; }
        
        public string currCategoryName { get; set; }
        public int[] pageNumbers { get; set; }
        public int selectedPage { get; set; }
    }
}
