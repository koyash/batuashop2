﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BatuaShop2.Models;

namespace BatuaShop2.ViewModels
{
    public class OneBookViewModel
    {
        public Book book { get; set; }
        
        public List<Book> sameBooks { get; set; }
        
        public bool isFavourite { get; set; }
    }
}
