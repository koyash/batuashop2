﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BatuaShop2.Models;

namespace BatuaShop2.ViewModels
{
    public class HomeViewModel
    {
        public IEnumerable<Book> favBooks { get; set; }
    }
}
