﻿using System.Collections.Generic;
using BatuaShop2.Models;

namespace BatuaShop2.ViewModels
{
    public class ManagerInfoViewModel
    {
        public List<Order> Orders { get; set; }
        public Users Users { get; set; }
    }
}