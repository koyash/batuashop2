﻿using System.Collections.Generic;
using System.Security.Claims;
using BatuaShop2.Models;

namespace BatuaShop2.ViewModels
{
    public class UserPageViewModel
    {
        public Users User;

        public List<Order> orders;
        
        public List<int> Repeats { get; set; }
    }
}