﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using BatuaShop2.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Internal;

namespace BatuaShop2.Data
{
    public class DbObjects
    {

        private const string managerEmail = "manager@gmail.com";

        public static void Initial(AppDbContent content)
        {
            if (!content.Category.Any())
            {
                content.Category.AddRange(Categories.Select(c => c.Value));
            }

            if (!content.Book.Any())
            {
                content.AddRange(
                    new Book
                    {
                        name = "it",
                        longDesc = "Страшная книга про клоуна",
                        shortDesc = "Книга про клоуна",
                        img = "/img/IT.jpg",
                        price = 100,
                        categoryName = "horror",
                        categoryId = 7
                    },
                    new Book
                    {
                        name = "кладбище домашних животных",
                        longDesc = "Кладбище бывает разным!",
                        shortDesc = "Страшная книга про волшебное кладбище",
                        img = "/img/1629832-1085966.jpg",
                        price = 150,
                        categoryName = "horror",
                        categoryId = 7
                    },
                    new Book
                    {
                        name = "В метре друг от друга",
                        longDesc = "Книга про школьников. Больных школьников",
                        shortDesc = "Книга про любовь",
                        img = "/img/cover1__w220.jpg",
                        price = 50,
                        categoryName = "romantic",
                        categoryId = 6
                    },
                    new Book()
                    {
                        name = "Метро 2033",
                        longDesc = "Книга о том, как мир погряз в постапокалиптической ядерной войне и как он выживает в 2033 году",
                        shortDesc = "Про апокалипсис",
                        img = "/img/metro.jpg",
                        price = 100,
                        categoryId = 5
                    },
                    new Book()
                    {
                        name = "Гордон рамзи",
                        longDesc = "Книга о том как крутой повар научит вас готовить!",
                        shortDesc = "Про готовку",
                        img = "/img/ramzi.jpg",
                        price = 250,
                        categoryId = 4
                    },
                    new Book()
                    {
                        name = "Первому игроку приготовиться",
                        longDesc = "В недалеком будощем весь мир погряз в экономический упадок. Главный герой выживает в волшебном мире",
                        shortDesc = "Про будущее",
                        img = "/img/firstPlayer.jpg",
                        price = 300,
                        categoryId = 5
                    }, 
                    new Book()
                    {
                        name = "Хребты безумия. Лавкрафт",
                        shortDesc = "Лучшее кошмарно-прекрасно-завораживающее произведение.",
                        longDesc = "Хребты безумия» написаны в документальной манере повествования, постепенно привыкая к которой, становишься свидетелем особой реальности описываемых событий. Полярная экспедиция сталкивается с запредельным ужасом древних веков. Лучшее кошмарно-прекрасно-завораживающее описание в творчестве Лавкрафта запечатлено именно в этом произведении. Это описание заброшенного ледяного города «древних», уводящего в недра земли.",
                        img = "/img/Lavcraft.png",
                        price = 413,
                        categoryId = 7
                    },
                    new Book()
                    {
                        name = "Коробка в форме сердца. Джо хилл",
                        longDesc = "Наш вам совет: никогда не покупайте через Интернет привидение. Тем более если к нему в нагрузку прилагается его любимый черный костюм. Иначе с вами может случиться в точности такая же история, какая произошла с Джудом Койном, знаменитым рок-музыкантом, в благословенной стране Америке. Попавшись на сетевую удочку и сделав злополучный заказ, однажды он получил посылку - коробку в форме сердца с вышеупомянутым костюмом внутри. Лучше бы он этого не делал...",
                        shortDesc = "Что же в ней?",
                        img = "/img/Heart.png",
                        price = 490,
                        categoryId = 7
                    },
                    new Book()
                    {
                        name = "Телепорт. Стивен Гулд",
                        longDesc = "Девид Райс, который практически не помнит своей матери, оставившей семью и исчезнувшей в неизвестном направлении. Вся злость и обида на жену вымещается отцом на юном парне. Деви в ожидании очередной взбучки закрывает глаза, мечтая оказаться в безопасном месте и однажды ему это удается – подняв веки он обнаруживает себя в другой части города.Теперь мальчишке удается перемещаться в пространстве при любой угрозе его покою. Он решает покинуть отчий дом, чтобы найти мать. Удастся ли Девиду совершить задуманное, особенно после того, как о его умении узнает беспощадный и опасный враг?",
                        shortDesc = "История про спидраннера вне времени и пространства ",
                        img = "/img/teleport.png",
                        price = 695,
                        categoryId = 5
                    },
                    new Book()
                    {
                        name = "Слияние. Дженнифер Уэлс",
                        shortDesc = "Знакомство с инопланетянами и выход в межгалактический конгломерат",
                        longDesc = "Есть ли жизнь за пределами Земли? Узнать об этом можно из книг про инопланетян. Команда ученых во главе с доктором Джейн Холлоуэй отправляется к центру галактики – планете Терак, однако по пути корабль сталкивается с Кай’Мемном, после чего требует длительного восстановления. В тоже время дочь одного из специалистов НАСА по имени Зара добирается до секретной информации и представляет ее на всеобщее обозрение в интернет.Девочка без труда учит инопланетный язык и возглавляет команду таких же как она, особенных подростков, которых правительства по всему миру принимают на работу в качестве переводчиков на общегалактический язык. Космические войны уже давно угрожают гибелью Земле, однако благодаря детям у планеты появляется шанс на выживание.",
                        img = "/img/sliyanie.png",
                        price = 780,
                        categoryId = 5
                    },
                    new Book()
                    {
                        name = "Заповедник Гоблинов. Клиффорд Саймак",
                        longDesc = "В далеком будущем человечество совершит немало удивительных изобретений, например, станут возможны межпланетные путешествия. В одном из таких путешествий Питер Максвелл, профессор факультета Сверхъестественных явлений, случайно попадает на таинственную Хрустальную планету - необъятное хранилище редких знаний. Местные жители готовы поделиться с ним этими знаниями... но не просто так, а в обмен на некий Артефакт. Заповедник гоблинов - один из лучших романов Клиффорда Саймака, воспевающий веру в разум человека!",
                        shortDesc = "Самые обыкновенные чудеса",
                        img = "/img/goblins.png",
                        price = 340,
                        categoryId = 5
                    },
                    new Book()
                    {
                        name = "Гарри Поттер и методы рационального мышления. Элизер Юдковский",
                        longDesc = "Петуния вышла замуж не за Дурсля, а за университетского профессора, и Гарри попал в гораздо более благоприятную среду. У него были частные учителя, дискуссии с отцом, а главное — книги, сотни и тысячи научных и фантастических книг. В 11 лет Гарри знаком с квантовой механикой, когнитивной психологией, теорией вероятностей и другими вещами. Но Гарри не просто вундеркинд, у него есть загадочная Тёмная сторона, которая явно накладывает свой отпечаток на его мышление.",
                        shortDesc = "Вундеркиннд, или иная история мальчика, который выжил",
                        img = "/img/Harry.png",
                        price = 400,
                        categoryId = 5
                    },
                    new Book()
                    {
                        name = "Маленький принц. Антуан де Сент Экзюпери",
                        longDesc = "Самое знаменитое произведение Антуана де Сент-Экзюпери с авторскими рисунками. Мудрая и человечная сказка-притча, в которой просто и проникновенно говорится о самом важном: о дружбе и любви, о долге и верности, о красоте и нетерпимости к злу.Все мы родом из детства, - напоминает великий француз и знакомит нас с самым загадочным и трогательным героем мировой литературы.",
                        shortDesc = "Ведь все взрослые сначала были детьми, только мало кто из них об этом помнит",
                        img = "/img/little.png",
                        price = 253,
                        categoryId = 3
                    },
                    new Book()
                    {
                        name = "Красная шапочка. Шарль Перро",
                        shortDesc = "Из-за того, что девочка шла к бабушке через лес, ее бабулю съел волк",
                        longDesc = "Она получила такое прозвище из-за подарка своей бабушки – шапки из красного бархата. Девочке так понравился новый головной убор, что она стала носить его, не снимая. Поэтому её так и прозвали – Красная шапочка. Девочка была доброй, отзывчивой, болтливой, смелой, но очень доверчивой и легкомысленной.",
                        img = "/img/redGirl.png",
                        price = 100,
                        categoryId = 3
                    },
                    new Book()
                    {
                        name = "Галлюцинации. Оливер Сакс",
                        longDesc = "Галлюцинации. В Средние века их объясняли духовным просветлением или, напротив, одержимостью дьяволом. Десятки людей были объявлены святыми, тысячи сгорели на кострах инквизиции. \r\nВ наше время их принято считать признаком сумас-шествия, тяжелой болезни или следствием приема наркоти¬ческих средств. Но так ли это?\r\nВы крепко спите в своей комнате и внезапно просыпаетесь от резкого звонка в дверь. Вскочив, вы подходите к двери, но там никого нет. Наверное, показалось, — думаете вы, не догадываясь, что это была типичная галлюцинация. Какая галлюцинация? Ведь я же не сумасшедший!В своей новой работе Оливер Сакс обращается к миру галлюцинаций, и, как всегда, главную ценность книги пред¬ставляют реальные истории людей, вступивших в упорную борьбу за возвращение к психическому здоровью и полно¬ценной жизни!..",
                        shortDesc = "Галлюцинации – просветление или одержимость нечистью?",
                        img = "/img/saks.png",
                        price = 230,
                        categoryId = 2
                    },
                    new Book()
                    {
                        name = "Человек для себя. Эрих Формм",
                        shortDesc = "Что остается от человека, если отбросить физический труд?",
                        longDesc = "В работе Человек для себя Эрих Фромм рассматривает предназначение человека в современном мире, освободившем его от тяжелого физического труда. Развивая материальную культуру, человечество получило власть над природой, однако при всех своих знаниях о материи человек остался невежественен в самых важных, основополагающих вопросах человеческого бытия: что такое человек, как он должен жить и куда он может направить ту огромную энергию, что заключена в нем самом?",
                        img = "/img/dlyaSebya.png",
                        price = 196,
                        categoryId = 2
                    },
                    new Book()
                    {
                        name = "1984. Дж. Оруэл",
                        longDesc = "Своеобразный антипод второй великой антиутопии XX века - О дивный новый мир Олдоса Хаксли. Что, в сущности, страшнее: доведенное до абсурда общество потребления - или доведенное до абсолюта общество идеи?\r\nПо Оруэллу, нет и не может быть ничего ужаснее тотальной несвободы...",
                        shortDesc = "Антиутопия о тяжелых временах",
                        img = "/img/oryel.png",
                        price = 286,
                        categoryId = 2
                    },
                    new Book()
                    {
                        name = "Бэтмэн. Эго. Д.Кук",
                        longDesc = "Бэтмен живет в мире насилия, и насилие — движущая сила его мотивов. Он готов восстанавливать справедливость любой ценой. Уверенность в правильности этого метода дает трещину, после того как на глазах Темного Рыцаря один из приспешников Джокера убивает себя. Герой, не ведающий страха и сомнений, впадает в депрессию и добровольно заточает себя в Бэт-пещере. Он не покинет ее, пока не разберется в себе и не поймет, что на самом деле движет им, что управляет его «я» — страх или желание защищать, жажда мести или стремление к справедливости?",
                        shortDesc = "Супергерой запутался в своих мотивах спасать мир.",
                        img = "/img/ego.png",
                        price = 215,
                        categoryId = 1
                    },
                    new Book()
                    {
                        name = "Психушка Джокера. Заагадочник. Карточный домик. П.Кэллоуэй",
                        longDesc = "Однажды Загадочник отправился грабить художественную галерею. Пока он крал картину, прекрасная Джессика Дюшан похитила его сердце. Загадочник влюбился с первого взгляда. Снова и снова он пытается завладеть вниманием и сердцем красавицы, но получает отказ. В чем причина? Пустив в ход все свое хитроумие, Нигма приходит к мысли: она ответит взаимностью, если он откажется от преступной жизни и, изменив себя, станет ближе к Джессике. Очень долго Эдвард строит на обломках прежней жизни нового себя, и вот когда девушка оказывается в его объятиях... О, в историях Джокера финал — самая забавная часть! Смешно будет до боли...",
                        shortDesc = "Смешно будет до боли...",
                        img = "/img/zagadochnik.png",
                        price = 151,
                        categoryId = 1
                    },
                    new Book()
                    {
                        name = "Призрак в доспехах. С.Масамунэ",
                        longDesc = "Классика киберпанка, всемирно известная манга Сиро Масамунэ (настоящее имя мангаки - Масанори Ота) о деятельности Девятого отдела, ведущего в Японии первой трети XXI века борьбу с кибер- и просто террористами, – одно из важнейших произведений, представляющих новую реальность ближайшего будущего, когда машинный интеллект и бессмертная человеческая душа станут единым в «доспехах» сознания. Начав выходить в мае 1989 года в журнале Young Magazine, манга обрела статус культовой, вдохновив многих подражателей. Сам же автор вдохновлялся произведением Артура Кёслера «Дух в машине» (Ghost in the machine), которое, в свою очередь, вобрало в себя идеи Декарта о дуализме сознания и тела.По манге было снято знаменитое аниме 1995 года, разработаны компьютерные игры.",
                        shortDesc = "Машинный интеллект и бессмертная человеческая душа станут единым в «доспехах» сознания",
                        img = "/img/ghost.png",
                        price = 861,
                        categoryId = 1
                    }

                );
            }

            content.SaveChanges();

        }
        
        private static Dictionary<string, Category> category;

        public static Dictionary<string, Category> Categories
        {
            get
            {
                if (category == null)
                {
                    var list = new Category[]
                    {
                        new Category{categoryName = "Ужасы", desc = "Books that will scare you"},
                        new Category{categoryName = "Романтика", desc = "Books that will show true love"},
                        new Category{categoryName = "Научная фантастика", desc = "Some fantastic books"},
                        new Category{categoryName = "Готовка", desc = "book about how to cook"},
                        new Category{categoryName = "Детские", desc = "books for childs"},
                        new Category{categoryName = "Популярное", desc = "some famous books"},
                        new Category{categoryName = "Комиксы", desc = "interesting comics"}
                    };
                    category = new Dictionary<string, Category>();
                    foreach (Category item in list)
                    {
                        category.Add(item.categoryName, item);
                    }
                }
                return category;
            }
        }

    }
}
