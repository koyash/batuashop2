﻿using BatuaShop2.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BatuaShop2
{
    public class AppDbContent : IdentityDbContext
    {
        public AppDbContent(DbContextOptions<AppDbContent> options) : base(options)
        {

        }

        public DbSet<Category> Category { get; set; }
        public DbSet<Book> Book { get; set; }
        public DbSet<ShopCartItem> ShopCartItem { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderDetail> OrderDetail { get; set; }
        
        public DbSet<Users> Users { get; set; }
        
        public DbSet<Email> Email { get; set; }
        
        public DbSet<Favourites> Favourites { get; set; }
        
        public DbSet<IdentityRole> Roles { get; set; }
        
        public DbSet<IdentityUserRole<string>> UserRoles { get; set; }
        
        
        
    }
}
