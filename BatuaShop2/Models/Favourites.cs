﻿namespace BatuaShop2.Models
{
    public class Favourites
    {
        public int id { get; set; }
        public string userId { get; set; }
        public int bookId { get; set; }
    }
}