﻿using System;
namespace BatuaShop2.Models
{
    public class ShopCartItem
    {
        public int id { get; set; }
        public Book book { get; set; }
        public uint price { get; set; }
        public uint count { get; set; }

        public String shopCartId { get; set; }
    }
}
