﻿using Microsoft.AspNetCore.Identity;

namespace BatuaShop2.Models
{
    public class Users : IdentityUser
    {
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        
    }
}