﻿using System;
using System.Collections.Generic;
namespace BatuaShop2.Models
{
    public class Category
    {
        public int id { set; get; }
        public String categoryName { set; get; }
        public String desc { set; get; }
    }
}