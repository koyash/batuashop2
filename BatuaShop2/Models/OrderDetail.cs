﻿

namespace BatuaShop2.Models
{
    public class OrderDetail
    {
        public int id { get; set; }
        public int orderId { get; set; }
        public int bookId { get; set; }
        public uint price { get; set; }
        public virtual Book book { get; set; }
    }
}