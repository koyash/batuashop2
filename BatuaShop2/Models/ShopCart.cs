﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BatuaShop2.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BatuaShop2.Models
{
    public class ShopCart
    {

        private readonly AppDbContent appDbContent;
        private static IServiceProvider _serviceProvider;

        public ShopCart(AppDbContent appDbContent)
        {
            this.appDbContent = appDbContent;
        }


        public string ShopCartId { get; set; }
        public static List<ShopCartItem> listShopItems { get; set; }

        public static ShopCart GetCart(IServiceProvider services)
        {
            _serviceProvider = services; 
            if (listShopItems == null)
                listShopItems = new List<ShopCartItem>();
            ISession session = services.GetRequiredService<IHttpContextAccessor>()?.HttpContext.Session;
            var context = services.GetService<AppDbContent>();
            string shopCartId = session.GetString("CartId") ?? Guid.NewGuid().ToString();

            session.SetString("CartId", shopCartId);

            return new ShopCart(context) { ShopCartId = shopCartId };

        }

        public void AddToCart(Book book)
        {
            var listItems = getShopItems();
            bool found = false;
            ShopCartItem foundItem = null;
            foreach (var item in listItems)
            {
                if (item.book.id == book.id)
                {
                    item.count++;
                    foundItem = item;
                    found = true;
                }
            }
            if (!found)
            {
                var item = new ShopCartItem
                {
                    shopCartId = ShopCartId,
                    book = book,
                    price = book.price,
                    count = 1
                };
                this.appDbContent.ShopCartItem.Add(item);
                listShopItems.Add(item);
            }
            else
            {
                this.appDbContent.ShopCartItem.Update(foundItem);
                listShopItems.Add(foundItem);
            }
            appDbContent.SaveChanges();
        }
        public List<ShopCartItem> getShopItems()
        {
            return appDbContent.ShopCartItem.Where(c => c.shopCartId == ShopCartId).Include(s => s.book).ToList();
        }

        public void ClearShopItems()
        {
            ISession session = _serviceProvider.GetRequiredService<IHttpContextAccessor>()?.HttpContext?.Session;
            
            session?.Remove("CartId");
            
            listShopItems.Clear();
        }
    }
}
