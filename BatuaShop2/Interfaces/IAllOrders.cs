﻿

using System.Threading.Tasks;
using BatuaShop2.Models;

namespace BatuaShop2.Interfaces
{
    public interface IAllOrders
    {

        Task CreateOrder(Order order);

        Task<bool> CloseOrder(int id);
    }
}
