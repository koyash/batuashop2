﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BatuaShop2.Models;

namespace BatuaShop2.Interfaces
{
    public interface IFavourite
    {
        public Task<List<Favourites>> getFavourites(string userId);
        public Task<bool> addFavourite(string userId, int bookId);
    }
}