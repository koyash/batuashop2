﻿using System.Collections.Generic;

namespace BatuaShop2.Interfaces
{
    public interface IAllBooks
    {
        IEnumerable<Models.Book> Books { get; }
        Models.Book getObjectBook(int bookId);
        void RemoveFromFavourite(int bookId);
        void AddToFavourite(int bookId);
        List<Models.Book> getBooksByName(string name);
    }
}
