﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BatuaShop2.Interfaces
{
    public interface IBookCategory
    {
        IEnumerable<Models.Category> allCategories { get; }

    }
}
