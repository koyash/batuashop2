﻿using System.Threading.Tasks;
using BatuaShop2.Models;

namespace BatuaShop2.Interfaces
{
    public interface IEmail
    {
        public Task<bool> SaveEmail(Email email);
    }
}